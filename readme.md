## Contraktor

Aplicativo realizado como parte de processo seletivo feito em React (Front-end) e Laravel (API).

O que ele não realiza:

- Autenticação de usuários.
- Validação dos campos de formulários.
- Validação dos campos de busca.
- Validação das datas de início e de término de contrato
- Validação do tipo de arquivo a ser submetido via upload.
- Busca complexa no banco de dados.
- Não foi utilizada a persistência de dados através de Mobx, Redux, Local Storage, etc. Os dados estão sendo recuperados diretamente do banco de dados.

O que ele realiza:

- Cadastro de partes e contratos.
- Upload de arquivo de pdf/docx como parte do cadastro de contrato.
- Visualização do arquivo submetido via upload.
- Recuperação e visualização das partes cadastradas.
- Recuperação e visualização dos contratos cadastrados.
- Recuperação do relacionamento entre as partes e os contratos.

## Etapas para a instalação

Através do terminal clone o repositório com um dos dois comando abaixo:
```
git@gitlab.com:josielqo/contraktor.git

https://gitlab.com/josielqo/contraktor.git
```
Entre no diretório onde o projeto foi clonado e através do terminal execute os seguinte comandos:
```
composer install
```
```
npm install
```
```
npm run dev
```
Crie um banco de dados para hospedar os dados da aplicação.

Copie o arquivo .env.example, mude seu nome para .env, e altere os seguintes dados:

```
DB_DATABASE= "nome do banco de dados criado"

DB_USERNAME= "usuário do banco de dados"

DB_PASSWORD= "senha do banco de dados"
```

No terminal execute os seguintes comandos:

``` 
php artisan key:generate
```

``` 
php artisan migrate
```

``` 
php artisan storage:link
```

Ao executar o próximo comando, será habilitado um servidor no endereço http://127.0.0.1:8000 onde será possível acessar o aplicativo através dele.

``` 
php artisan serve
```

## Orientações

Ao iniciar o aplicativo o banco de dados estará em branco.

Para o cadastro de items, todos os campos devem ser preenchidos.

Após ter algum item cadastrado será possível pesquisá-lo. Como o campo de busca está sem validação, caso clique no botão de buscar sem que algo tenha sido digitado, serão retornados todos os registros.

A busca se realiza mediante o clique do botão de busca. Não está habilitada busca mediante via comando "enter" do teclado. 

Para se realizar a busca é necessário digitar exatamente a mesma grafia com que o item foi cadastrado, ou pelo menos, com algumas palavras sequenciais tal qual foi cadastrado:

```
Adesão ao plano de saúde
```

Para recuperá-lo deve-se digitar:

```
Adesão ao plano de saúde
```
Não se deve digitar por exemplo:

```
adesão plano
```