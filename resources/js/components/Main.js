import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { ConfigContext } from './ConfigContext';
import { BrowserRouter, Route, Link, Redirect } from 'react-router-dom';
import Contract from './Contract';
import Party from './Party';
import SearchParty from './SearchParty';
import ListParty from './ListParty';
import SearchContract from './SearchContract';
import ListContract from './ListContract';
import Loading from './Loading';

class Main extends Component {
  constructor(props){
    super(props)
    this.state={
      loading: false,
      config: {
        urlBase: "http://127.0.0.1:8000"
      }
    }
    this.setLoading = this.setLoading.bind(this)
  }

  setLoading(status){
    this.setState({
      loading: !this.state.loading
    })
  }

  render() {
    return ( 
      <BrowserRouter>
        <ConfigContext.Provider value={this.state.config}>
        {this.state.loading && <Loading/>}  
        <div>
            <nav className="navbar navbar-light navbar-expand-md navigation-clean">
                <div className="container"><Link to="/" className="navbar-brand" >Contraktor</Link><button data-toggle="collapse" className="navbar-toggler" data-target="#navcol-1"><span className="sr-only">Toggle navigation</span><span className="navbar-toggler-icon"></span></button>
                    <div className="collapse navbar-collapse"
                        id="navcol-1">
                        <ul className="nav navbar-nav ml-auto">
                          <li className="nav-item" role="presentation">
                            <Link className="nav-link" to="/party">Cadastrar Partes</Link>
                          </li>
                          <li className="nav-item" role="presentation">
                            <Link className="nav-link" to="/search-party">
                              Pesquisar Partes
                            </Link>
                          </li>
                          <li className="nav-item" role="presentation">
                            <Link className="nav-link" to="/contract">
                              Cadastrar Contratos
                            </Link>
                          </li>
                          <li className="nav-item" role="presentation">
                            <Link className="nav-link" to="/search-contract">
                              Pesquisar Contratos
                            </Link>
                          </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div> 
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <main>
                
                <Route exact path="/" component={Party}/>

                <Route exact path="/contract" render={(props) => <Contract {...props} toogleLoading={this.setLoading} />}/>
                
                <Route exact path="/search-party" render={(props) => <SearchParty {...props} toogleLoading={this.setLoading} />}/>
                
                <Route exact path="/search-contract" render={(props) => <SearchContract {...props} toogleLoading={this.setLoading} />}/>
                
                <Route exact path="/contract/:action" component={OperationContract}/>
                
                <Route exact path="/contract/:action/:id" render={(props) => <OperationContract {...props} toogleLoading={this.setLoading} />} />
                
                <Route exact path="/party" render={(props) => <Party {...props} toogleLoading={this.setLoading} />}/>
                
                <Route exact path="/party/:action" component={OperationParty}/>
                
                <Route exact path="/party/:action/:id" render={(props) => <OperationParty {...props} toogleLoading={this.setLoading} />}/>
              </main>
            </div>
          </div>
        </div>
        </ConfigContext.Provider>
      </BrowserRouter>
    );
  }
}

const OperationContract = ({match,toogleLoading}) => {
  const action = match.params.action
  const id     = match.params.id
  return(
    <div>
      {action == "list" ? <ListContract loading={toogleLoading} list={id}/> : <Contract/>}
    </div>
  )
}

const OperationParty = ({match,toogleLoading}) => {
  const action = match.params.action
  const id     = match.params.id
  return(
    <div>
      {action == "list" ? <ListParty loading={toogleLoading} list={id}/> : <Party/>}
    </div>
  )
}

if (document.getElementById('root')) {
    ReactDOM.render( < Main /> , document.getElementById('root'));
}
