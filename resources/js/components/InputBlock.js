import React from 'react';

const InputBlock = ({inputLabel,inputName,value,inputChange})=>(
  <div className="form-group files">
    <label>{inputLabel}</label>
    <input type="text" name={inputName} className="form-control" value={value} onChange={inputChange}/>
  </div>
)
 
export default InputBlock