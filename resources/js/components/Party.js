import React, { Component, Fragment } from 'react';
import { ConfigContext } from './ConfigContext';
import axios from 'axios';
import InputBlock from './InputBlock'

class Party extends Component {

    static contextType = ConfigContext;

    constructor(props) {
      super(props);
      this.state ={
        cpf: '',
        name: '',
        lastname: '',
        mail: '',
        phone: '', 
        parties: [],
        searchInput: '',
        search: [],
        msgText: '',
        error: false,
        saved: false,
        isDisabledButton: true
      }
      this.handleSearchChange = this.handleSearchChange.bind(this)
      this.handleSubmitSearch = this.handleSubmitSearch.bind(this)
      this.handleSubmit       = this.handleSubmit.bind(this)
      this.handleInputChange  = this.handleInputChange.bind(this)
    }

    componentDidMount(){
      let id = this.props.id ? this.props.id : ''
      if(this.props.id){
        let url = `${this.context.urlBase}/api/parties/${id}`
        axios.get(url)
        .then((response) => {
          this.setState({
            cpf: response.data.cpf,
            name: response.data.name,
            lastname: response.data.lastname,
            mail: response.data.mail,
            phone: response.data.phone
          })
        });
      }

      let urlParties  = `${this.context.urlBase}/api/parties`
      axios.get(urlParties)
      .then((response) => {
        this.setState({
          parties: response.data
        })
      })

    }

    handleSearchChange(event) {
      const target = event.target;
      const value = target.value
      this.setState({searchInput: value})
    }
  
    handleSubmitSearch(e){
      e.preventDefault()
      const {search} = this.state 
      let url  = `http://127.0.0.1:8000/api/search/party?q=${search}`
      axios.get(url)
        .then((response) => {
          this.setState({search: response.data})
        })
    }

    handleInputChange(event) {
      const target = event.target;
      const value = target.type === 'checkbox' ? target.checked : target.value;
      const nameState = target.name;
  
      this.setState({
        [nameState]: value
      },()=>{
        const {cpf,
          name,
          lastname,
          mail,
          phone} = this.state

          if(cpf != '' && name !== '' && lastname !== '' && mail !== '' && phone !== ''){
            this.setState({
              isDisabledButton: false
            });
          }else{
            this.setState({
              isDisabledButton: true
            });
          }
      });

    }
  
    handleSubmit(e){
      e.preventDefault() 
      this.props.toogleLoading()
      let url  = `${this.context.urlBase}/api/parties`
      const {cpf, name, lastname, mail, phone} = this.state
      
      let data      = {}
      data.cpf      = cpf 
      data.name     = name
      data.lastname = lastname
      data.mail     = mail
      data.phone    = phone

      axios.post(url, data)
      .then((response) => {
        if(response.status == 201){
          this.props.toogleLoading()
          jQuery("input").val('')
          this.setState ({
            cpf: '',
            name: '',
            lastname: '',
            mail: '',
            phone: '',
            error: false,
            saved: true,
            msgText: 'Cadastrado com sucesso!',
            isDisabledButton: true
          })
        }
      }).catch((response) => {
        this.props.toogleLoading()
          this.setState ({
            saved: false,
            error: true,
            msgText: 'Não foi possível efetuar o cadastro!!' 
          })
      })
    }
    
    render() {
      const {cpf, name, lastname, mail, phone, isDisabledButton, msgText, saved, error} = this.state
      const buttonStatus =  isDisabledButton ? 'disabled' : ''
      const savedClass = "success" 
      const nonSavedClass = "error" 
      return (
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <div className="hero">
              <h1 className="hero__title">Cadastrar uma <strong>parte</strong></h1>
            </div>
            <div className="msg">
              {saved && 
                <div className={`msg__text ${savedClass}`}>
                  {msgText && msgText}
                </div>
              }
              {error && 
                <div className={`msg__text ${nonSavedClass}`}>
                  {msgText && msgText}
                </div>
              }
            </div>
            <form>

              <InputBlock inputLabel="CPF" 
                          inputName="cpf" 
                          value={cpf} 
                          inputChange={this.handleInputChange}/>

              <InputBlock inputLabel="Nome" 
                          inputName="name" 
                          value={name} 
                          inputChange={this.handleInputChange}/>

              <InputBlock inputLabel="Sobrenome" 
                          inputName="lastname" 
                          value={lastname} 
                          inputChange={this.handleInputChange}/>

              <InputBlock inputLabel="E-mail" 
                          inputName="mail" value={mail} 
                          inputChange={this.handleInputChange}/>

              <InputBlock inputLabel="Telefone" 
                          inputName="phone" 
                          value={phone} 
                          inputChange={this.handleInputChange}/>

              <div className="col-md-12 pull-right">
              <button disabled={buttonStatus} width="100%" type="button" className="btn btn-info" onClick={this.handleSubmit}>
                Cadastrar
              </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}


 
export default Party;