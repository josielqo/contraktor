import React, { Component } from 'react';
import { ConfigContext } from './ConfigContext';
import axios from 'axios';
import moment from 'moment';
import Select from 'react-select';
import Calendar from './Calendar';
import InputBlock from './InputBlock'

class Contract extends Component {

  static contextType = ConfigContext;

  constructor(props) {
    super(props);
    this.state ={
      contractTitle: '',
      contractStart: moment().format('YYYY-MM-DD'),
      contractEnd: moment().format('YYYY-MM-DD'),
      pdf_file: '',
      document: '',
      parties: [],
      selectedParty: null,
      msgText: '',
      error: false,
      saved: false,
      isDisabledButton: true
    }
    this.onChangeHandler = this.onChangeHandler.bind(this)
    this.submit = this.submit.bind(this)
    this.handleInputChange = this.handleInputChange.bind(this)
    this.onChangeSelect = this.onChangeSelect.bind(this)
    this.getStartDateCalendar = this.getStartDateCalendar.bind(this)
    this.getEndDateCalendar = this.getEndDateCalendar.bind(this)
    this.changeStatusButton = this.changeStatusButton.bind(this)
    this.select = React.createRef()
    this.calendarStart = React.createRef()
    this.calendarEnd = React.createRef()
  }
  
  /*
  =============================================================================
  */
  
  componentDidMount(){

    this.props.toogleLoading()
    let urlParties = `${this.context.urlBase}/api/parties`
    axios.get(urlParties)
    .then((response) => {
      this.props.toogleLoading()
      this.setState({
        parties: response.data
      })
    });
  }

  /*
  =============================================================================
  */

  handleInputChange(event) {
    const target = event.target
    const value = target.type === 'checkbox' ? target.checked : target.value
    const name = target.name
    this.setState({[name]: value},()=>this.changeStatusButton())
  }

  /*
  =============================================================================
  */

  onChangeSelect(selectedParty){
    this.setState({ selectedParty },()=>this.changeStatusButton());
  };

  /*
  =============================================================================
  */

  onChangeHandler(e) {
    
    let files = e.target.files || e.dataTransfer.files;
    if (!files.length)
    return;
    this.setState({
      document: files[0]
    },()=>this.changeStatusButton())
  }
  /*
  =============================================================================
  */

  changeStatusButton(){
    const {contractTitle, contractStart, contractEnd, parties, document} = this.state
    if(contractTitle != '' && contractStart !== '' && contractEnd !== '' && document !== '' && parties !== ''){
      this.setState({
        isDisabledButton: false
      });
    }else{
      this.setState({
        isDisabledButton: true
      });
    }
  }

  /*
  =============================================================================
  */
  
  submit(){
    this.props.toogleLoading()
    const {document, contractStart, contractEnd, contractTitle, selectedParty} = this.state
    
    const data = new FormData()
    data.append('file', document)
    data.append('pdf_file', document)
    data.append('start_at', contractStart)
    data.append('end_at', contractEnd)
    data.append('title', contractTitle)
    data.append('pivot', JSON.stringify(selectedParty))
    
    const url = `${this.context.urlBase}/api/contracts`

    axios.post(url, data)
    .then((response) => {
      if(response.status == 201){
        this.props.toogleLoading()
        $("input").val(null)
        this.select.current.setState({
          value: null
        })
        this.calendarStart.current.setState({
          startDate: new Date()
        })
        this.calendarEnd.current.setState({
          startDate: new Date()
        })
        this.setState ({
          contractTitle: '',
          contractStart: moment().format('YYYY-MM-DD'),
          contractEnd: moment().format('YYYY-MM-DD'),
          error: false,
          saved: true,
          msgText: 'Cadastrado com sucesso!',
          isDisabledButton: true 
        })
      }
    })
    .catch((response) => {
      this.props.toogleLoading()
        this.setState ({
          saved: false,
          error: true,
          msgText: 'Não foi possível efetuar o cadastro!!' 
        })
    })
  }

  /*
  =============================================================================
  */

  getStartDateCalendar(e){
    this.setState({
      contractStart:  moment(e).format('YYYY-MM-DD')
    })
  }

  /*
  =============================================================================
  */

  getEndDateCalendar(e){
    this.setState({
      contractEnd:  moment(e).format('YYYY-MM-DD')
    })
  }

  /*
  =============================================================================
  */
  
  render() {
    const {parties, isDisabledButton, contractTitle, saved, error, msgText} = this.state
    const options = parties.map(function(option){
      return {value: option.id, label: option.name}
    })
    const buttonStatus =  isDisabledButton ? 'disabled' : ''
    const savedClass = "success" 
    const nonSavedClass = "error"
    return (
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <div className="hero">
              <h1 className="hero__title">Cadastrar <strong>contrato</strong></h1>
            </div>
            <div className="msg">
              {saved && 
                <div className={`msg__text ${savedClass}`}>
                  {msgText && msgText}
                </div>
              }
              {error && 
                <div className={`msg__text ${nonSavedClass}`}>
                  {msgText && msgText}
                </div>
              }
            </div>
            <form>

              <InputBlock inputLabel="Título" 
                          inputName="contractTitle"
                          value={contractTitle}
                          inputChange={this.handleInputChange}/>

              <div className = "form-group files">
                <label> Partes </label> 
                {
                  <Select
                    ref={this.select}
                    name="parties_in_contract"
                    isMulti
                    isSearchable
                    options={options}
                    onChange={this.onChangeSelect}
                  />
                }
              </div> 
              <div className = "form-group files">
                <label> Começa em </label> 
                <Calendar ref={this.calendarStart} getDate={this.getStartDateCalendar}/>
              </div> 
              <div className = "form-group files">
                <label> Termina em </label> 
                <Calendar ref={this.calendarEnd} getDate={this.getEndDateCalendar}/>
              </div> 
              <div className = "form-group files">
                <label>Anexe o contrato</label> 
                <input type = "file" name = "file" className = "form-control" onChange = {this.onChangeHandler}/> 
              </div> 
              <div className = "col-md-12 pull-right">
                <button disabled={buttonStatus} type = "button" className = "btn btn-info" onClick = {this.submit} >Cadastrar</button>
              </div> 
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default Contract;


 

  

